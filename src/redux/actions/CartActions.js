import * as types from "./../constants/CartTypes";
import callAPI, { API_URL } from "../../apiCaller/config";
import Axios from "axios";

export const fetchCart = () => {
    return (dispatch) => {
        var cart = JSON.parse(localStorage.getItem('cart'));
        if (cart !== null && cart !== undefined) {
            dispatch(actFetchCart(cart));
        }
        else {
            localStorage.removeItem('cart');
            dispatch(actFetchCart([]));
        }
    }
}

export const actFetchCart = (cart) => {
    return {
        type: types.FETCH_CART,
        cart,
    }
}

export const pushNewItemIntoCart = (pid) => {
    return (dispatch) => {
        return callAPI(`products/${pid}`, 'GET', null).then(pro => {
            let cartItem = {
                product: pro.data,
                quantity: 1
            }
            dispatch({
                type: types.PUSH_NEW_ITEM_INTO_CART,
                cartItem: cartItem
            });
        });
    }
}

export const updateCartItemQuantity = (itemID, amount, isAddition) => {
    return (dispatch) => {
        let cart = JSON.parse(localStorage.getItem('cart'));
        let itemToUpdate = {};
        for (const item of cart) {
            if (item.product.id === itemID) {
                itemToUpdate = item;
                break;
            }
        }        
        if (isAddition) {
            
            itemToUpdate.quantity += parseInt(amount);
        }
        else {
            itemToUpdate.quantity = amount;
        }
        dispatch(actUpdateQuantity(itemToUpdate));
    }
}

export const actUpdateQuantity = (product) => {
    return {
        type: types.UPDATE_CART_ITEM_QUANTITY,
        cartItem: product
    }
}

export const deleteCart = () => {
    return (dispatch) => {
        dispatch({
            type: types.DELETE_CART,
        });
    }
}

export const deleteOneItem = (itemID) => {
    return (dispatch) => {
        dispatch({
            type: types.DELETE_ONE_ITEM,
            itemID
        });
    }
}

export const checkout = (itemID) => { // ID?
    return (dispatch) => {
    let cart = JSON.parse(localStorage.getItem('cart'));
    for (const item of cart) {
        item.paid = true;
        callAPI(`cartitem/${item.id}`, 'PUT', item);
    }
    return dispatch(actCheckout());
    }
}

export const actCheckout = () => {
    return {
        type: types.CHECK_OUT,
    }
}
