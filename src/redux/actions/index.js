import * as types from "../constants/ActionTypes";
import callAPI from "../../apiCaller/config";

export const actFetchProductsRequest = () => {
    return (dispatch) => {
        return callAPI('products', 'GET', null).then(res => {
            dispatch(actFetchProducts(res.data));
        });
    }
}

export const actFetchProducts = (products) => {
    return {
        type: types.FETCH_PRODUCTS,
        products
    }
}

// export const fetchOneProduct = (id) => {
//     return (dispatch) => {
//         return callAPI(`products/${id}`, 'GET', null).then(res => {
//             dispatch(actFetchOneProduct(res.data));
//         });
//     }
// }

// export const actFetchOneProduct = (product) => {
//     return {
//         type: types.FETCH_ONE_PRODUCT,
//         product
//     }
// }

export const actFetchBrandProductsRequest = (brand) => {
    return (dispatch) => {
        return callAPI('products', 'GET', null).then(res => {
            dispatch(actFetchBrandProducts(brand, res.data));
        });
    }
}

export const actFetchBrandProducts = (brand, products) => {
    return {
        type: types.FETCH_BRAND_PRODUCTS,
        brand,
        products
    }
}