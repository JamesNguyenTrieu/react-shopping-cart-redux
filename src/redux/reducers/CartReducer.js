import * as types from "./../constants/CartTypes";

// const cartFromLC = localStorage.getItem('cart');
// var total = 0;
// if (cartFromLC) {
//     for (const item of cartFromLC) {
//         total += (item.product.price * item.quantity);
//     }
// }
// // const cartItemsFromLC = localStorage.getItem('cartitems');
// var initialState = {
//     cart: cartFromLC ? JSON.parse(cartFromLC) : [],
//     total
//     // products: cartItemsFromLC ? JSON.parse(cartItemsFromLC) : []
// };

var initialState = {
    cart: [],
    total: 0
}

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_CART:
            state.cart = action.cart;
            state.total = totalCalculation(state.cart);
            
            return {
                cart: [...state.cart],
                total: state.total
            };
        case types.PUSH_NEW_ITEM_INTO_CART:
            state.cart.push(action.cartItem);
            localStorage.setItem('cart', JSON.stringify(state.cart));

            return {
                cart: [...state.cart],
                total: state.total
            };
        case types.UPDATE_CART_ITEM_QUANTITY:
            for (const item of state.cart) {
                if (item.product.id === action.cartItem.product.id) {
                    item.quantity = action.cartItem.quantity;
                    break;
                }
            }
            state.total = totalCalculation(state.cart);
            localStorage.setItem('cart', JSON.stringify(state.cart));
            return {
                cart: [...state.cart],
                total: state.total
            }
        case types.DELETE_CART:
            state.cart = [];
            state.total = 0;
            localStorage.removeItem('cart');
            return {
                cart: [],
                total: state.total
            }
        case types.DELETE_ONE_ITEM:
            for( let i = 0; i < state.cart.length; i++){
                if ( state.cart[i].product.id === action.itemID) {
                    state.cart.splice(i, 1);
                    break;
                }
            }
            state.total = totalCalculation(state.cart);
            if (state.cart.length > 0) {
                localStorage.setItem('cart', JSON.stringify(state.cart));
            }
            else {
                localStorage.removeItem('cart');
            }            
            return {
                cart: [...state.cart],
                total: state.total
            }
        // case types.CHECK_OUT:
        //     state.cart.slice(0, state.cart.length);
        //     state.products.slice(0, state.products.length);
        //     state.total = 0;
        //     localStorage.removeItem('cart');
        //     localStorage.removeItem('cartitems');
        //     return {
        //         cart: [],
        //         products: [],
        //         total: state.total
        //     }
        default:
            return state;
    }
}

const totalCalculation = (cart) => {
    var total = 0;
    for (const item of cart) {
        total += (item.product.price * item.quantity);
    }
    return total;
}

export default cartReducer;