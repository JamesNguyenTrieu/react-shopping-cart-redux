import { combineReducers } from 'redux';
import productReducer from "./products";
import navBarReducer from "./NavBarReducer";
import cartReducer from "./CartReducer";

const myReducer = combineReducers({
    productReducer,
    cartReducer,
    navBarReducer,
});

export default myReducer;