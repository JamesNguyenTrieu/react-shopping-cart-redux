import * as types from "./../constants/ActionTypes";

var initialState = {
    products: [],
    brands: [],
    // detailedProduct: {}
};

const productReducer = (state = initialState, action) => {
    switch(action.type) {
        case types.FETCH_PRODUCTS:
            state.products = action.products;
            var brands = []
            for (const product of state.products) {
                var existed = false;
                for (const brand of brands) {
                    if (product.brand.toUpperCase() === brand) {
                        existed = true;
                        break;
                    }
                }
                if (!existed) {
                    brands.push(product.brand.toUpperCase());
                }
            }
            state.brands = brands.sort();
            localStorage.setItem('brands', JSON.stringify(state.brands));
            return {
                products: [...state.products],
                brands: [...state.brands]
            };
        case types.FETCH_BRAND_PRODUCTS: 
            state.products = action.products; 
            let pros = [];
            for (const product of state.products) {
                if (product.brand === action.brand) {
                    pros.push(product);
                }
            }            
            return {
                products: [...pros],
                brands: [...state.brands]
            };
        // case types.FETCH_ONE_PRODUCT:
        //     state.detailedProduct = action.product;
        default: return state;
    }
}

export default productReducer;