import axios from "axios";
import Swal from "sweetalert2";
// export const API_URL = 'http://5dfc44390301690014b902fd.mockapi.io';
export const API_URL = 'http://localhost:4000';

export default function callAPI(endpoint, method = 'GET', body) {
    return axios({
        method: method,
        url: `${API_URL}/${endpoint}`,
        data: body
    }).catch(err => {
        console.log(err);        
        Swal.fire({
            type: 'error',
            title: 'Opps! Something went wrong',
            text: err
        });     
    })
}