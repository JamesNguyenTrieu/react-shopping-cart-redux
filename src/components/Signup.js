import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import callAPI from '../apiCaller/config';
import { connect } from 'react-redux';
import * as actions from "./../redux/actions/NavActions";

export class Signup extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             username: '',
             password: '',
             retypedPassword: ''
        }
    }

    componentDidMount() {
        this.checkURL();
    }
    
    checkURL = () => {
        let navBarShowed = true;
        let currentPage = window.location.href;
        currentPage = (currentPage).slice(currentPage.lastIndexOf("/")+1, currentPage.length);
        if (currentPage === 'signup' || currentPage === 'login') {
            navBarShowed = false;
        }

        this.props.hideOrShowNavBar(navBarShowed);
    }

    labelStyle = {
        float: "left"
    }

    onUsernameChange = (e) => {
        this.setState({
            ...this.state,
            username: e.target.value
        });
    }

    onPasswordChange = (e) => {
        this.setState({
            ...this.state,
            password: e.target.value
        });
    }

    onPasswordRetyping = (e) => {
        this.setState({
            ...this.state,
            retypedPassword: e.target.value
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        if (this.state.password !== this.state.retypedPassword) {
            Swal.fire({
                type: 'error',
                title: "Incorrect retyped password!",
                text: 'Please try again'
            });
        }
        else {

            let data = {
                username: this.state.username,
                password: this.state.retypedPassword
            }
            callAPI('users', 'GET', null)
            .then(res => {
                if (this.isUserNameExisted(res.data, data.username, data.password)) {
                    Swal.fire({
                        type: 'error',
                        title: 'Duplicated username!',
                        text: 'This username is existed. Please try another one!'
                    });
                }
                else {
                    callAPI('users', 'POST', data)
                    .then(res => {                
                        Swal.fire({
                            position: 'top',
                                type: 'success',
                                title: 'Signed up successfully!',
                                showConfirmButton: false,
                                timer: 2000
                        });
                        localStorage.setItem('user', this.state.username);
                        localStorage.setItem('userid', res.data.id);
                        this.props.hideOrShowNavBar(true);
                        this.props.history.push("/");
                    });
                }
            });
            
        }
    }

    isUserNameExisted = (userArr, us, pwd) => {
        for (const user of userArr) {
            if (user.username === us && user.password === pwd)
                return true;
        }
        return false;
    }

    render() {
        var user = localStorage.getItem("user");
        if (user !== null) {
            return <Redirect to="/" />
        }
        return (
            <div className="container">
                
                <div className="row">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                        <div className="panel panel-warning">
                              <div className="panel-heading">
                                    <legend className="panel-title">
                                        <h3>Signup</h3>
                                    </legend>
                              </div>
                              <div className="panel-body">                                
                                    <form onSubmit={this.onSubmit}>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Username:</label>
                                            <input type="text" className="form-control" onChange={this.onUsernameChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Password:</label>
                                            <input type="password" className="form-control" onChange={this.onPasswordChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Retyped password:</label>
                                            <input type="password" className="form-control" onChange={this.onPasswordRetyping}/>
                                        </div>

                                        <button type="submit" className="btn btn-success">Create my account</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button type="reset" className="btn btn-danger">Reset</button>
                                    </form>
                                    <br></br>
                                    <Link to='/login'>You've had an account? Click here to log in!</Link>
                              </div>
                        </div>
                        
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                    </div>
                </div>
                
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        isShowed: state.navBarReducer.isShowed
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hideOrShowNavBar: (status) => {
            dispatch(actions.hideOrShowNavBar(status));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
