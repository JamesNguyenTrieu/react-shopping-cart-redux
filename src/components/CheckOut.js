import React, { Component } from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import './CheckOut.scss';
// import TotalPriceForCheckout from './TotalPriceForCheckout';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from "./../redux/actions/CartActions";

export class CheckOut extends Component {
    constructor(props) {
        super(props)
    
        this.submit = this.submit.bind(this);
        this.state = {
            fullname: '',
            phoneNumber: '',
            shippingAddress: '',
        }
    }
    
    componentDidMount() {
        this.props.fetchCart();
    }

    async submit(ev) {
        ev.preventDefault();
        let {token} = await this.props.stripe.createToken({name: "Name"});
        if (token !== null && token !== undefined) {
            const body = {
                fullname: this.state.fullname,
                phoneNumber: this.state.phoneNumber,
                shippingAddress: this.state.shippingAddress,
                totalCost: this.props.total,
                token: token.id
            }

            //  let response = await fetch("/charge", {
            //  method: "POST",
            //  headers: {"Content-Type": "text/plain"},
            //  body: body
            //  });

            // if (response.ok) Swal... (below)

            console.log(body);  
            Swal.fire({
                type: 'success',
                title: 'Purchase completed successfully!',
                text: 'Thank you for buying. Have a wonderful day!'
            }).then((result) => {              
                if (result.value) {
                    this.props.checkout();
                }
              });
        }
    }
    getTotalPrice = (total) => {
        this.setState({
            cost: parseInt(total)
        })
    }

    onFullnameChange = (e) => {
        this.setState({
            ...this.state,
            fullname: e.target.value
        });
    }

    onPhoneNumberChange = (e) => {
        this.setState({
            ...this.state,
            phoneNumber: e.target.value
        });
    }

    onShippingAdressChange = (e) => {
        this.setState({
            ...this.state,
            shippingAddress: e.target.value
        });
    }
    
    render() {
        const cart = localStorage.getItem('cart');
        if (cart === null || cart === undefined || JSON.parse(cart).length === 0) {
            return <Redirect to="/" />
        }
        return (
            <div className="checkout">
                <form onSubmit={this.submit}>
                    <div className="form-group">
                        <label>Fullname:</label>
                        <input type="text" className="form-control" required onChange={this.onFullnameChange}/>
                    </div>
                    <div className="form-group">
                        <label>Phone number:</label>
                        <input type="text" className="form-control" required onChange={this.onPhoneNumberChange}/>
                    </div>
                    <div className="form-group">
                        <label>Shipping address:</label>
                        <input type="text" className="form-control" required onChange={this.onShippingAdressChange}/>
                    </div>
                    {/* <div className="form-group">
                        <TotalPriceForCheckout sendTotal={this.getTotalPrice}></TotalPriceForCheckout>
                    </div> */}
                    <div className='form-group'>
                        <label>Total cost:</label>
                        <span className="label label-warning">${this.props.total}</span>
                    </div>
                    <CardElement className='card' total={this.getTotalPrice}/>
                    <button type="submit" className="btn btn-success">Purchase</button>
                </form>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        total: state.cartReducer.total
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCart: () => {
            dispatch(actions.fetchCart());
        },
        checkout: () => {
            dispatch(actions.checkout());
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectStripe(CheckOut));
