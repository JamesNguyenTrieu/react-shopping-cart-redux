import React, { Component, Fragment} from 'react';
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as productActions from "../redux/actions";
import * as navBarActions from "./../redux/actions/NavActions";
import * as cartActions from "./../redux/actions/CartActions";
import "./NavigationBar.scss";
import Swal from 'sweetalert2';

export class NavigationBar extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             cartIsEmpty: true
        }
        this.checkLoginAndURL();
        this.props.fetchCart();
    }
    
    componentDidMount() {
        this.props.fetchAllProducts();
    }

    chooseBrand = (e) => {
        this.props.fetchBrandProducts((e.target.text).toLowerCase());
    }

    getAllProducts = () => {
        this.props.hideOrShowNavBar(true);
        this.props.fetchAllProducts();
    }

    checkLoginAndURL = () => {
        this.props.hideOrShowNavBar(true);
        this.props.setLoginStatus(false);
    }

    onDeleteOneItem = (itemID) => {
        this.props.deleteOneItemFromCart(itemID.toString());
    }

    onClickDelete = () => {
        if (this.props.cart.length > 0) {
            Swal.fire({
                title: 'Are you sure?',
                text: 'All items in your cart is going to be deleted!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, I am!'
            }).then((result) => {
                if (result.value) {
                    this.props.deleteCart();
                }
            });
        }
    }

    onChangeItemQuantity = (e, itemID) => {
        let p = {};
        for (const iter of this.props.cart) {
            if (iter.product.id == itemID) {
                p = iter;
                break
            }
        }

        if ( parseInt(e.target.value) > p.product.quantity) {
            Swal.fire({
                type: 'error',
                title: 'Not enough products in storage!',
                text: 'Please enter a smaller number of quantity!'
            });
            return;
        }
        
        if (parseInt(e.target.value) <= 0 || e.target.value === null || e.target.value === '') {
            Swal.fire({
                type: 'error',
                title: 'Quantity must be filled and positive',
                text: 'Please try again!'
            });
            return;
        }
        else {
            this.props.updateExistingItemQuantity(itemID, parseInt(e.target.value), false);
        }
    }    

    handleInputFocus = (e) => {
        e.target.select();
    }

    render() {
        var showAllBrands = this.props.allBrands.map( (brand, index) => {
            return (
                <li key={index} className="navLink" onClick={this.chooseBrand}><Link to={`/brands/${brand.toLowerCase()}`}>{brand}</Link></li>
            )
        });     
        var numberOfCartItems = 0;
        for (const iterator of this.props.cart) {
            numberOfCartItems += iterator.quantity;
        }

        return (
            this.props.navBarIsShowed ?
            <div>
                <nav className="navbar navbar-default" role="navigation">
                    <div className="navbar-header">
                        <Link className="navbar-brand" to="/" onClick={this.getAllProducts}><i className="fab fa-glide-g"></i><b>Electronics</b></Link>
                    </div>
                    <div className="collapse navbar-collapse navbar-ex1-collapse">
                        <ul className="nav navbar-nav left">
                            <li className="navLink" onClick={this.getAllProducts}><Link to="/">All Products</Link></li>
                            {
                                showAllBrands
                            }
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li className='cart-icon'><Link to=""><i className="fas fa-shopping-cart"></i></Link></li>
                            <div className='mini-cart'>
                                <div className="count-and-delete-all">
                                    <div className='count'>
                                        <h5>YOUR CART</h5>
                                        <p>({numberOfCartItems} items)</p>
                                    </div>
                                    <button type="button" className="btn btn-danger deleteAll" onClick={this.onClickDelete}>Delete all items</button>
                                </div>
                                <table className="table table-hover">
                                    <tbody>
                                        {
                                            this.props.cart.length ?
                                            this.props.cart.map( item => 
                                                <tr key={item.product.id}>
                                                    <td className="img">
                                                        <img src={item.product.image} alt=""/>
                                                    </td>
                                                    <th className="name-delete">
                                                        <div className="name">{item.product.name}</div>
                                                        <div className="delete" onClick={() => this.onDeleteOneItem(item.product.id)}><i className="fas fa-trash-alt"></i> Delete from cart</div>
                                                    </th>
                                                    <td></td>
                                                    <th className="price">
                                                        ${ (item.product.price * item.quantity).toFixed(2) }
                                                    </th>
                                                    <td className="quantity">
                                                        <input type="number" min="1" max={item.product.quantity} value={item.quantity} onClick={this.handleInputFocus} onChange={(e) => this.onChangeItemQuantity(e, item.product.id)}/>
                                                    </td>
                                                </tr>
                                            ) :
                                            <tr>
                                                <td className='empty-cart'>
                                                    <img src="https://www.jivanayurveda.com/images/empty-cart.png" alt=""/>
                                                </td>
                                            </tr>
                                        }
                                    </tbody>
                                </table>
                                <div className='payment'>
                                    <div className='text'>
                                        <h4>Total: </h4>
                                        <h4 className='total'>${ (this.props.total).toFixed(2) }</h4>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                </nav>
            </div> : 
            <div>
                <nav className="navbar navbar-default" role="navigation">
                    <div className="navbar-header">
                        <Link className="navbar-brand" to="/" onClick={this.getAllProducts}><i className="fab fa-glide-g"></i>Electronics</Link>
                    </div>
                </nav>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        products: state.productReducer.products,
        allBrands: state.productReducer.brands,
        navBarIsShowed: state.navBarReducer.isShowed,
        cart: state.cartReducer.cart,
        total: state.cartReducer.total
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllProducts: () => {
            dispatch(productActions.actFetchProductsRequest());
        },
        fetchBrandProducts: (brand) => {
            dispatch(productActions.actFetchBrandProductsRequest(brand));
        },
        hideOrShowNavBar: (status) => {
            dispatch(navBarActions.hideOrShowNavBar(status));
        },
        setLoginStatus: (status) => {
            dispatch(navBarActions.setLoginStatus(status));
        },
        fetchCart: () => {
            dispatch(cartActions.fetchCart());
        },        
        deleteOneItemFromCart: (itemID) => {
            dispatch(cartActions.deleteOneItem(itemID));
        },
        deleteCart: () => {
            dispatch(cartActions.deleteCart());
        },
        updateExistingItemQuantity: (itemID, amount, isAddition) => {
            dispatch(cartActions.updateCartItemQuantity(itemID, amount, isAddition));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar)
