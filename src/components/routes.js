import { React } from "react";
import Product from "./Product";
import NotFound from './NotFound';
import DetailedProduct from "./DetailedProduct";

const routes = [
    {
        path: '',
        exact: true,
        main: (match) => {
            return <Product match={match}/>
        }
    },
    {
        path: '/products/',
        exact: true,
        main: (match) => {
            return <Product match={match}/>
        }
    },
    {
        path: '/products/:id/',
        exact: true,
        main: () => {
            return <DetailedProduct/>
        }
    },
    {
        path: '',
        exact: false,
        main: () => {
            return <NotFound/>
        }
    },
]

export default routes;