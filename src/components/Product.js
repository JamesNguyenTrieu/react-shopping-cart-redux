import React, { Component } from 'react';
import { connect } from "react-redux";
import * as productActions from "../redux/actions";
import * as cartActions from "./../redux/actions/CartActions";
import "./Product.scss";
import Swal from 'sweetalert2';

export class Product extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            brandName: ''
        }
        this.props.fetchAllProducts();
        this.props.fetchCart();
    }

    componentDidMount() {
        let brands = JSON.parse(localStorage.getItem('brands'));
        let URLBrandName = this.props.match.params.brandName;
        if (URLBrandName && URLBrandName !== undefined && URLBrandName !== "") {
            let existed = false;
            for (const item of brands) {
                if (item.toLowerCase() === URLBrandName) {
                    existed = true;
                    break;
                }
            }
            if (existed) {
                this.setState({
                    brandName: this.props.match.params.brandName
                });
                this.props.fetchBrandProducts(this.props.match.params.brandName);
            } else {
                this.props.history.push('/notfound');
            }
            
        }
    }

    addToCart = (pid) => {
        let cart = JSON.parse(localStorage.getItem('cart'));
        if (cart !== null){
            var existedItem = null;
            for (const item of cart) {
                if (item.product.id == pid) {
                    existedItem = item;
                    break;
                }
            }
            if (existedItem !== null) {
                if (existedItem.quantity < existedItem.product.quantity) {
                    this.props.updateExistingItemQuantity(existedItem.product.id, 1, true);
                    this.cartNotification(`Updated this item's quantity in your cart!`);
                }
                else {
                    Swal.fire({
                        type: 'error',
                        title: 'Not enough products in storage!',
                        text: 'You have all of this type of product in your cart!'
                    });
                    return;
                }
            }
            else {
                this.props.pushIntoCart(pid);
                this.cartNotification('Added to cart successfully!');
            }
        }
        else {
            this.props.pushIntoCart(pid);
            this.cartNotification('Added to cart successfully!');
        }
    }

    cartNotification = (message) => {
        Swal.fire({
            position: 'top',
            type: 'success',
            title: message,
            showConfirmButton: false,
            timer: 1000
        });
    }

    userHasNotLoggedIn = () => {
        this.props.hideOrShowNavBar(false);
    }

    render() {
        var result = this.props.products.map((product, index) => {
            let cartItem = null;
            for (const iter of this.props.cart) {
                if (iter.product.id === product.id) {
                    cartItem = iter;
                    break;
                }
            }

            if (cartItem !== null) {
                if (cartItem.quantity >= product.quantity) {
                    return (
                        <div className="item" key={index}>
                            <img src={product.image} className="img-responsive" alt={product.name}/>
                            <h4 className="product-name">{product.name}</h4>
                            <h4>
                                ${ (product.price).toFixed(2) }
                            </h4>
                            <div className="add-to-cart">
                                <button type='button' className="btn btn-warning add-to-cart-button disabled">Out of stock</button>
                            </div>
                        </div>
                    )
                }
            }
            
            return (
                <div className="item" key={index}>
                        <img src={product.image} className="img-responsive" alt={product.name}/>
                        <h4 className="product-name">{product.name}</h4>
                        <h4>
                            ${ (product.price).toFixed(2) }
                        </h4>
                    <div className="add-to-cart">
                        <button type='button' className={product.quantity > 0 ? "btn btn-warning add-to-cart-button" : "btn btn-warning add-to-cart-button disabled"} onClick={() => this.addToCart(product.id)}>{product.quantity > 0 ? "Add to Cart" : "Out of stock"}</button>
                    </div>
                </div>
            )
        });
        return (
            
            <div className="container">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 list">
                        {result}
                    </div>
            </div>
       
        )
    }
}

const mapStateToProps = state => {
    return {
        products: state.productReducer.products,
        brands: state.productReducer.brands,
        cart: state.cartReducer.cart
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllProducts: () => {
            dispatch(productActions.actFetchProductsRequest());
        },
        fetchBrandProducts: (brand) => {
            dispatch(productActions.actFetchBrandProductsRequest(brand));
        },
        fetchCart: () => {
            dispatch(cartActions.fetchCart());
        },
        pushIntoCart: (productID) => {
            dispatch(cartActions.pushNewItemIntoCart(productID));
        },
        updateExistingItemQuantity: (itemID, amount, isAddition) => {
            dispatch(cartActions.updateCartItemQuantity(itemID, amount, isAddition));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);
