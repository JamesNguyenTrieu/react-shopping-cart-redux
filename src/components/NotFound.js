import React, { Component } from 'react'

export class NotFound extends Component {
    myStyle = {
        width: '1000px',
        height: '490px',
        margin: "auto"
    }
    render() {
        return (
            <div>
                <h2>404 - Page not found!</h2>
                <img style={this.myStyle} src="https://previews.123rf.com/images/trueffelpix/trueffelpix1706/trueffelpix170600020/81135346-oops-sorry-business-man-vector-illustration.jpg" className="img-responsive" alt="" />
            </div>
        )
    }
}

export default NotFound
