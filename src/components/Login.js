import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import callAPI from '../apiCaller/config';
import Swal from 'sweetalert2';
import * as actions from "./../redux/actions/NavActions";
import { connect } from 'react-redux';

export class Login extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             username: '',
             password: ''
        }
    }

    componentDidMount() {
        this.checkURL();
    }

    checkURL = () => {
        let navBarShowed = true;
        let currentPage = window.location.href;
        currentPage = (currentPage).slice(currentPage.lastIndexOf("/")+1, currentPage.length);
        if (currentPage === 'signup' || currentPage === 'login') {
            navBarShowed = false;
        }

        this.props.hideOrShowNavBar(navBarShowed);
    }

    getUsername = (e) => {
        this.setState({
            ...this.state,
            username: e.target.value
        });
    }

    getPassword = (e) => {
        this.setState({
            ...this.state,
            password: e.target.value
        });
    }

    isValidAccount = (userArr, us, pwd) => {
        for (const user of userArr) {
            if (user.username === us && user.password === pwd)
                return user.id;
        }
        return -1;
    }
    
    onLogIn = (e) => {
        e.preventDefault();
        callAPI('users', 'GET', null)
        .then(res => {
            let us = this.isValidAccount(res.data, this.state.username, this.state.password)
            if (us !== -1) {
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'Logged in successfully!',
                    showConfirmButton: false,
                    timer: 2000
                });
                localStorage.setItem('user', this.state.username);
                localStorage.setItem('userid', us);
                this.props.hideOrShowNavBar(true);
                this.props.history.push("/");
            }
            else
                Swal.fire({
                    type: 'error',
                    title: 'Wrong credentials!',
                    text: 'Please try again'
                });
        });
    }
    render() {        
        var user = localStorage.getItem("user");
        if (user !== null) {
            return <Redirect to="/" />
        }
        return (
            <div className="container">
                
                <div className="row">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                        <div className="panel panel-warning">
                              <div className="panel-heading">
                                    <legend className="panel-title">
                                        <h3>Login</h3>
                                    </legend>
                              </div>
                              <div className="panel-body">
                                    
                                    <form onSubmit={this.onLogIn}>                                    
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder="Username" required onChange={this.getUsername}/>
                                        </div>
                                        <div className="form-group">
                                            <input type="password" className="form-control" placeholder="Password" required onChange={this.getPassword}/>
                                        </div>

                                        <button type="submit" className="btn btn-success">Log in</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button type="reset" className="btn btn-danger">Reset</button>
                                    </form>
                                    <br></br>
                                    <Link to="/signup">You not have an account? Sign up here!</Link>
                              </div>
                        </div>
                        
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        
                    </div>
                </div>
                
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        isShowed: state.navBarReducer.isShowed
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        hideOrShowNavBar: (status) => {
            dispatch(actions.hideOrShowNavBar(status));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);